call plug#begin('~/.config/nvim/plugged')
  " Syntax
  Plug 'tpope/vim-git'
  Plug 'othree/html5.vim'
  Plug 'digitaltoad/vim-pug'
  Plug 'pangloss/vim-javascript'
  Plug 'posva/vim-vue'
  Plug 'elzr/vim-json'
  Plug 'wavded/vim-stylus'
  Plug 'stanangeloff/php.vim', { 'for': 'php' }
  Plug 'stephpy/vim-yaml', { 'for': 'yaml' }
  Plug 'fatih/vim-go', { 'for': 'go' }
  Plug 'derekwyatt/vim-scala', { 'for': 'scala' }
  Plug 'derekwyatt/vim-sbt', { 'for': 'sbt' }

  " Color Schemes
  Plug 'morhetz/gruvbox'
  Plug 'dracula/vim'

  " Lint
  Plug 'w0rp/ale'

  " Autocomplete
  Plug 'ervandew/supertab'
  Plug 'shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

  " Utilities
  Plug 'raimondi/delimitmate'
  Plug 'tpope/vim-surround'
  Plug 'tpope/vim-commentary'
  Plug 'tpope/vim-fugitive'
  Plug 'mattn/emmet-vim', { 'on': 'Emmet' }
  Plug 'vim-airline/vim-airline'
  Plug 'scrooloose/nerdtree'
  Plug 'ctrlpvim/ctrlp.vim'
  Plug 'airblade/vim-gitgutter'
  Plug 'yggdroot/indentline'
  Plug 'tpope/vim-repeat'
  Plug 'bronson/vim-trailing-whitespace'
  Plug 'vim-scripts/grep.vim'
call plug#end()

set number
set relativenumber
set fileformat=unix
set fileformats=unix,dos,mac
set fileencoding=utf-8
set fileencodings=utf-8
set expandtab
set tabstop=2
set shiftwidth=2
set nowrap
set list listchars=eol:♩,tab:››,trail:·,extends:»,precedes:«
set incsearch
set ignorecase
set smartcase
set hlsearch
set termguicolors
colorscheme dracula

" Airline
let g:airline_powerline_fonts = 1
let g:airline_skip_empty_sections = 1
let g:airline#extensions#ale#enabled = 1

" Autocomplete
let g:deoplete#enable_at_startup = 1
let g:SuperTabMappingForward = '<S-Tab>'
let g:SuperTabMappingBackward = '<Tab>'

" Indentation
vmap < <gv
vmap > >gv
let g:indentLine_char = '┆'

" Move Code
vnoremap J :m '>+1<CR>gv=gv
vnoremap K :m '<-2<CR>gv=gv

" Search
nnoremap <silent> <Leader><Space> :noh<CR>

" Git
noremap <Leader>gs :Gstatus<CR>
noremap <Leader>gc :Gcommit<CR>
noremap <Leader>gd :Gvdiff<CR>
noremap <Leader>ga :Gwrite<CR>

" Panels
noremap <Leader>h :<C-u>split<CR>
noremap <Leader>v :<C-u>vsplit<CR>
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l

" NerdTree
noremap <Leader>nt :NERDTreeToggle<CR>
let g:NERDTreeIgnore = ['node_modules', 'vendor', 'dist']
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

" CtrlP
let g:ctrlp_map = '<C-p>'
let g:ctrlp_cdm = 'CtrlP'
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']

" ALE
let g:ale_sign_column_always = 1
let g:ale_sign_error = '✗'
let g:ale_sign_warning = '⚠'

" JSON
let g:vim_json_syntax_conceal = 0

" Vim Plug
noremap <Leader>pi :PlugInstall<CR>
noremap <Leader>pu :PlugUpdate<CR>
noremap <Leader>pc :PlugClean!<CR>

" Markdown
let g:vim_markdown_folding_disabled = 1

" Tabs
nnoremap <Tab> gt
nnoremap <S-Tab> gT
nnoremap <silent> <S-T> :tabnew<CR>

" Grep
nnoremap <silent> <Leader>f :Rgrep<CR>
let Grep_Default_Options = '-IR'
let Grep_Skip_Files = '*.log'
let Grep_Skip_Dirs = '.git node_modules vendor dist target'
